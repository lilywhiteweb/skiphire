odoo.define('skiphire.website_sale', function (require) {
    'use strict';

    var core = require('web.core');
    var config = require('web.config');
    var publicWidget = require('web.public.widget');
    var VariantMixin = require('sale.VariantMixin');
    var wSaleUtils = require('website_sale.utils');
    const wUtils = require('website.utils');
    require("web.zoomodoo");

publicWidget.registry.WebsiteSaleSkipHire = publicWidget.Widget.extend(VariantMixin, {
    selector: '.oe_website_sale',
    events: _.extend({}, VariantMixin.events || {}, {
        'change select[name="state_id"]': '_onChangeState',
    }),
    init: function () {
        this._super.apply(this, arguments);

        this._changeState = _.debounce(this._changeState.bind(this), 500);

    },

    /**
     * @private
     */
    _changeState: function () {
        var country_id = $("#country_id").val();
        var state_id = $("select[name='state_id']" ).val();

        if (!$("select[name='state_id']").val()) {
            return;
        }
        this._rpc({
            route: "/shop/state_infos/" + country_id +"/" + state_id,
            params: {
                mode: $("#country_id").attr('mode'),
            },
        }).then(function (data) {
            var selectAreas = $("select[name='area_id']");

            if (selectAreas.data('init')===0 || selectAreas.find('option').length===1) {
                if (data.areas.length) {
                    selectAreas.html('');
                    _.each(data.areas, function (x) {
                        var opt = $('<option>').text(x[1])
                            .attr('value', x[0])
                            .attr('data-code', x[1]);
                        selectAreas.append(opt);
                    });
                    selectAreas.parent('div').show();
                } else {
                    selectAreas.val('').parent('div').hide();
                }
                selectAreas.data('init', 0);
            } else {
                selectAreas.data('init', 0);
            }

        });
    },

    /**
     * @private
     * @param {Event} ev
     */
    _onChangeState: function (ev) {
        if (!this.$('.checkout_autoformat').length) {
            return;
        }
        this._changeState();
    },
});

});
