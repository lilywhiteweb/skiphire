# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale


class CustomWebsiteSale(WebsiteSale):

    # New POST route to get the areas in each state
    @http.route(['/shop/state_infos/<model("res.country"):country>/<model("res.country.state"):state>'], type='json', auth="public", methods=['POST'], website=True)
    def state_infos(self, country, state, mode, **kw):
        return dict(
            fields=country.get_address_fields(),
            areas=[(ar.id, ar.name) for ar in country.get_website_sale_areas(state=state, mode=mode)],

        )


    # Override the route to add in the areas
    @http.route(['/shop/address'], type='http', methods=['GET', 'POST'], auth="public", website=True, sitemap=False)
    def address(self, **kw):
        res = super(CustomWebsiteSale, self).address(**kw)
        order = request.website.sale_get_order()
        partner_id = int(kw.get('partner_id', -1))

        # Add the areas to the qcontext for rendering
        if res.qcontext.get('country'):
            country = res.qcontext['country']
            res.qcontext['state_areas'] = country.get_website_sale_areas(state=order.partner_id.state_id)

        return res
