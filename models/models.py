# -*- coding: utf-8 -*-

from odoo import models, fields, api

# Override the Customer Model to include Area Field
class Customers(models.Model):
    _name = 'res.partner'
    _inherit = ['res.partner']

    area_id = fields.Many2one('res.country.state.area', string='Area', required=True)

    @api.model
    def _address_fields(self):
        res = super(Customers, self)._address_fields()
        res.append('area_id')
        return res

# New Area Model
class CountryStateArea(models.Model):
    _description = "State Area"
    _name = 'res.country.state.area'
    _order = 'name'

    country_state_id = fields.Many2one('res.country.state', string='State', required=True)
    name = fields.Char(string='Area Name', required=True,
               help='Administrative divisions of a State.')

# Override the CountryState Model to include the Area ID's
class CountryState(models.Model):
    _name = 'res.country.state'
    _inherit = ['res.country.state']

    area_ids = fields.One2many('res.country.state.area', 'country_state_id', string='Areas')


class CustomDeliveryCarrier(models.Model):
    _name = 'delivery.carrier'
    _inherit = ['delivery.carrier']

    area_ids = fields.Many2many('res.country.state.area', 'delivery_carrier_area_rel', 'carrier_id', 'area_id', 'Areas')

    @api.onchange('area_ids')
    def onchange_areas(self):
        self.state_ids = [(6, 0, self.state_ids.ids + self.area_ids.mapped('country_state_id.id'))]

    def _match_address(self, partner):
        res = super(CustomDeliveryCarrier, self)._match_address(partner)
        self.ensure_one()
        if self.area_ids and partner.area_id not in self.area_ids:
            res = False

        return res


class AreaCountry(models.Model):
    _inherit = ['res.country']

    def get_website_sale_areas(self, state, mode='billing'):

        res = self.env['res.country.state.area'].sudo().search([('country_state_id', '=', state.id)])

        return res
