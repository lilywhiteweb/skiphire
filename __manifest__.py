# -*- coding: utf-8 -*-
{
    'name': "Skip Hire",

    'summary': """
        Management of Skip Hire Orders""",

    'description': """
        Skip Hire Module for managing changes to order flow for a skip hire business
    """,

    'author': "Lily WHite Web",
    'website': "http://www.lilywhiteweb.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'delivery', 'website', 'sale', 'website_form'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
